#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

struct Pila{
    int valor;
    int tamano;
    struct Pila *siguiente;
    
};

struct nodoHash{
	char* respuestaC;
	char* respuestaI;
	char* llave;
	int id;
	struct nodoHash *siguiente;
};

struct Nodo{
    
    int valor;
    char *pregunta;
    struct Nodo *padre;
    struct Nodo *hijo_izq;
    struct Nodo *hijo_der;
    
};

//prototipos del programa
void push(int valor);
int pop();
void eliminarSplay(int valor);
void insertarSplay(int valor,char *pregunta);
void buscarSplay(int valor);
struct Nodo *iniciar_arbol(struct Nodo *padre, int valor, char *pregunta);
struct Nodo *rotacion_zig(struct Nodo *raiz, struct Nodo *nodo,int es_hijo_derecha);
struct Nodo *rotaciones(struct Nodo *nodo);
char* limpiar(char* str);
void comenzar_juego(struct nodoHash **tablaHash,int tam);
void cargar_datos();
void eliminar_nodo_vector(int numero);


int funcionHash(char* pregunta, int tam);
float factorDeCarga(struct nodoHash **v, int tam);
struct nodoHash **agrandar(struct nodoHash **v, int *tam);
void imprimeTabla(struct nodoHash **v, int tam);
char* limpiar(char* str);
struct nodoHash **insertar(struct nodoHash **v, int hash, int id, char* pregunta, char* respuestaC, char* respuestaI);
char** buscar(struct nodoHash **v, int tam, char* pregunta, int id);
struct nodoHash **agregar(struct nodoHash **v, int *tam);

//variables globales
struct Pila *pilaAct = NULL;
struct Nodo *arbol;
int tamanoPila = 0;
int vector_nodos_visitados[25];
int tamanoVectorVisita;
int vector_nodos_arbol[25];
int tamanoVectorArbol = 0;

int main(){
    /**
     * @brief funcion principal del programa
     * 
     * esta funcion inicializa las variables necesarias para el programa
     * crea la tabla de hash,  carga las preguntas al arbol y sale un menu
     * donde el usuario elige si quiere comenzar a jugar
     */
    int tam = 10;
    char* opcion = (char*)calloc(10,sizeof(char));
    
    struct nodoHash **tablaHash = (struct nodoHash**)calloc(sizeof(struct nodoHash),tam);
    for(int i = 0; i < tam; i++) tablaHash[i] = (struct nodoHash*)calloc(sizeof(struct nodoHash),1);
    int cont = 0;
	while(cont < tam){
		tablaHash[cont]->siguiente = NULL;
		tablaHash[cont]->respuestaC = NULL;
		tablaHash[cont]->respuestaI = NULL;
		tablaHash[cont]->id = -1;
		tablaHash[cont]->llave = NULL;
		cont++;
	}  
    
    tablaHash = agregar(tablaHash, &tam);
    
    for(int i = 0; i < 25; i++) vector_nodos_arbol[i] = i+1; 
    
    
    cargar_datos();
    printf("Bienvenido a la trivia mas emocionante de Juego De Tronos!\n");
    printf("(1) Empezar trivia\n");
    printf("(2) Salir\n");
    fgets(opcion,10,stdin);
    if(strcmp("1\n",opcion) == 0){
        
        comenzar_juego(tablaHash,tam);
    }
    else if(strcmp("2\n",opcion) == 0){
        printf("vuelve pronto!");
    }
    
    
    return 0;
}
void comenzar_juego(struct nodoHash **tablaHash,int tam){
    /**
     * @brief funcion que maneja el juego
     * @param struct nodoHash **tablaHash
     * @param int tam
     * 
     * la funcion maneja por completo el juego, maneja las rondas
     * que se estan jugando y las decisiones del usuario por si quiere 
     * dejar de jugar
     * 
     */
     
    int id_pregunta,ronda_visitas,victoria,seguir_jugando = 1;
    char* opcion = (char*)calloc(10,sizeof(char));
    char** respuestas = (char**)malloc(sizeof(char**) * 2);
	for(int i = 0; i < 2; i++) respuestas[i] = (char*)malloc(sizeof(char) *  200);
    srand(time(NULL));
    
    while(tamanoVectorArbol > 0 && seguir_jugando){

        id_pregunta = rand() % tamanoVectorArbol;
        id_pregunta = vector_nodos_arbol[id_pregunta];
        buscarSplay(id_pregunta);
        ronda_visitas = tamanoVectorVisita;
        victoria = 1;
        
        //**INICIANDO RONDA **//
        while(ronda_visitas > 0 && victoria){
            
            id_pregunta = vector_nodos_visitados[ronda_visitas-1];
            buscarSplay(id_pregunta);
            respuestas = buscar(tablaHash,tam,arbol->pregunta,id_pregunta);            
            
            system("clear");
            printf("\tTrivia De Juego De Tronos\n%s \n",arbol->pregunta);
            
            if(rand()%2){ //orden de las preguntas en la interfaz
                
                printf("(1)%s\n(2)%s",respuestas[0],respuestas[1]);
                printf("\ndigite su respuesta: ");
                fgets(opcion,10,stdin);
                
                if(strcmp("1\n",opcion) == 0){
                    printf("Correcto!, presiona enter para la siguiente pregunta");
                    getchar();
                }
                else{
                    printf("has perdido! quieres volver a jugar?\n(1) si (2) no\nrespuesta:");
                    fgets(opcion,10,stdin);
                    if(strcmp("2\n",opcion) == 0) seguir_jugando = 0;
                    victoria = 0;
                }
                
            }
            else{ // otro orden de pregunta en la interfaz
            
                printf("(1)%s\n(2)%s",respuestas[1],respuestas[0]);
                printf("\ndigite su respuesta: ");
                fgets(opcion,10,stdin);
                if(strcmp("2\n",opcion) == 0){
                    printf("Correcto!, presiona enter para la siguiente pregunta");
                    getchar();
                }
                else{
                    printf("has perdido! quieres volver a jugar?\n(1) si (2) no\nrespuesta:");
                    fgets(opcion,10,stdin);
                    if(strcmp("2\n",opcion) == 0) seguir_jugando = 0;
                    victoria = 0;
                }
            
            }//fin de orden de preguntas en la interfaz
            
            //**ELIMINACION DEL NODO**//
            eliminarSplay(id_pregunta);
            eliminar_nodo_vector(id_pregunta);
            ronda_visitas--;
        }
        /**FIN DE RONDA**/
        
        //**CONDICIONES DE JUEGO**//
        if(!victoria && seguir_jugando){
            arbol = NULL;
            for(int i = 0; i < 25; i++) vector_nodos_arbol[i] = i+1;
            tamanoVectorArbol = 0;
            cargar_datos();
            
            victoria = 1;
        }
        else if(victoria){
            system("clear");
            printf("has ganado la ronda quieres jugar la siguiente?\n");
            printf("(1) si (2) no\nrespuesta:");
            fgets(opcion,10,stdin);
            if(strcmp("2\n",opcion) == 0) seguir_jugando = 0,victoria = 0;
        }
        
        for(int i = 0; i < 25; i++) vector_nodos_visitados[i] = 0;
        tamanoVectorVisita = 0;
    }// fin de while juego
    
    //**FIN DE JUEGO**//
    system("clear");
    if(victoria){
        
        printf("Ya se han acabado todas las preguntas!\n");
        printf("has demostrado ser un verdadero fan de juego de tronos!\n");
        printf("gracias por jugar nuestra trivia!\n");
        printf("Enter para salir");
    }
    else{
    
        printf("Gracias por jugar nuestra trivia de juego de tronos!\n");
        printf("vuelve pronto!\n");
        printf("Enter para salir");
    }
}//fin de funcion

void eliminar_nodo_vector(int numero){
    /**
     * @brief elimna un nodo del vector de nodos
     * @param int numero
     * 
     * existe un vector de nodos para hacer un random sobre ese vector
     * y saber cuales seran las preguntas de la ronda, este metodo elimina
     * el nodo con la pregunta que acaba de pasar
     * 
     */
    for(int i = 0; i < vector_nodos_arbol[i]; i++){
        if(vector_nodos_arbol[i] == numero){
            vector_nodos_arbol[i] = vector_nodos_arbol[tamanoVectorArbol-1];
            vector_nodos_arbol[tamanoVectorArbol-1] = 0;
            tamanoVectorArbol--;
            break;
        }
    }
    
}


void cargar_datos(){
    /**
     * @brief carga datos del archivo
     * 
     * esta funcion carga las preguntas que hay en el archivo
     * y las va insertando en el arbol splay
     * 
     */
    
    char* datos_preguntas = (char*)calloc(200,sizeof(char));
    char* numero = (char*)calloc(200,sizeof(char));
    char* pregunta = (char*)calloc(200,sizeof(char));;
    FILE *f;
    f = fopen("preguntas.txt","r");
    
    while(!feof(f)){
        
        fgets(datos_preguntas,200,f);
        
        numero = strtok(datos_preguntas,"|");
        if(numero == NULL) break;
        pregunta    = strtok(NULL,"|");
        strtok(NULL,"|");
        strtok(NULL,"|");
        
        
        insertarSplay(atoi(numero),pregunta);
        
        datos_preguntas = limpiar(datos_preguntas);
        tamanoVectorArbol++;
    }
    
    
}


char* limpiar(char* str){
    /**
     * @brief limpia un string
     * @param char* str
     * pone un valor de \0 a todos los elementos
     * que hay en un string
     * 
     */
    for(int i = 0;i < strlen(str); i++) str[i] = '\0';
    return str;
}

void push(int valor){
    /**
     * @brief inserta nodo en pila
     * @param int valor
     * funcion push para la pila
     */
     
    struct Pila *insercion = calloc(sizeof(struct Nodo),1);
    insercion->siguiente = pilaAct;
    insercion->valor = valor;
    pilaAct = insercion;
}
int pop(){
    /**
     * @brief saca un nodo de la pila
     * 
     * funcion pop para la pila
     * 
     */
    int valor = pilaAct->valor;
    pilaAct = pilaAct->siguiente;
    return valor;

}


void insertarSplay(int valor,char *pregunta){
    /**
     * @brief inserta nodo en el arbol splay
     * @param int valor
     * @param char *pregunta
     * 
     * esta funcion inserta un nodo en el arbol splay
     * y hace las rotaciones al final
     */
     
    struct Nodo *nodo, *padre;
    int es_hijo_derecha;
    //INICIALIZO EL NODO SI EL ARBOL ES VACIO
    if(arbol == NULL){
        arbol = iniciar_arbol(NULL,valor,pregunta);
    }
    
    //**INSERCION DEL NODO**//
    else{
       nodo = arbol;
       while(nodo != NULL){
           padre = nodo;
           if(valor > nodo->valor){
                nodo = nodo->hijo_der;
                es_hijo_derecha = 1;
           }
           else{
                nodo = nodo->hijo_izq;
                es_hijo_derecha = 0;
           }
           push(es_hijo_derecha);
           tamanoPila++;
       }//fin de while
       
       nodo = iniciar_arbol(padre,valor,pregunta);
       if(es_hijo_derecha){
           
            padre->hijo_der = nodo;
       }
       else{
       
            padre->hijo_izq = nodo;
        }
    //**FIN DE INSERCION DEL NODO**//
               
        //****ROTACIONES****//
        arbol = rotaciones(nodo);
    }//else
}

void buscarSplay(int valor){
    /**
     * @brief busca un nodo en el arbol splay
     * @param int valor
     * 
     * busca el valor del parametro en el arbol splay
     * y luego hace las rotaciones del nodo encontrado hacia la raiz
     */
    struct Nodo *nodo;
    int es_hijo_derecha;
    if(arbol != NULL){
        
        nodo = arbol;
        
        while(nodo != NULL){
            
            vector_nodos_visitados[tamanoVectorVisita] = nodo->valor;
            tamanoVectorVisita++;
            if(nodo->valor == valor){
                arbol = rotaciones(nodo);
                return;
            }
            else if(valor > nodo->valor){
                nodo = nodo->hijo_der;
                es_hijo_derecha = 1;
            }
            else{
                nodo = nodo->hijo_izq;
                es_hijo_derecha = 0;
            }
                push(es_hijo_derecha);
                tamanoPila++;
       }//fin de while
    
       printf("NO SE ENCONTRO EL NODO");
        
    }
    printf("NO SE ENCONTRO EL NODO");
    while(tamanoPila > 0){
        pop();
        tamanoPila--;
    }
    
}

void eliminarSplay(int valor){
    /**
     * @brief elimina un nodo del arbol splay
     * @param int valor
     * 
     * busca el valor del parametro en el nodo y cuando el nodo
     * es encontrado se manda a la hoja mas lejana segun el algoritmo
     * de eliminacion de un nodo en un ABB, cuando llega a la hoja se elimina
     * y se manda al padre de esa hoja a la raiz
     * 
     */
    struct Nodo *nodo;
    struct Nodo *copia;
    int es_hijo_derecha;
    
    if(arbol != NULL){
        
        nodo = arbol;
        while(nodo != NULL){
            
            if(nodo->valor == valor){//se encontro el nodo
                copia = nodo;
                //**ELIMINANDO EL NODO**//
                //sigo haciendo este while hasta que el nodo llegue a ser una hoja
                while(copia != NULL){
                        
                        if(copia->hijo_der != NULL){//tiene 2 hijos o hijo derecho
                        //busco el menor de los mayores
                            push(1);
                            tamanoPila++;
                            copia = copia->hijo_der;
                            while(copia->hijo_izq != NULL){
                                push(0);
                                tamanoPila++;
                                copia = copia->hijo_izq;
                            }
                        nodo->valor = copia->valor;
                        nodo->pregunta = strcpy(nodo->pregunta,copia->pregunta);
                        nodo = copia;
                        }
                
                        else if(copia->hijo_izq != NULL){//tiene hijo izquierdo
                            //busco el mayor de los menores
                            push(0);
                            tamanoPila++;
                            copia = copia->hijo_izq;
                            while(copia->hijo_der != NULL){
                                push(1);
                                tamanoPila++;
                                copia = copia->hijo_der;
                            }
                            nodo->valor = copia->valor;
                            nodo->pregunta = strcpy(nodo->pregunta,copia->pregunta);
                            nodo = copia;
                        }
                        else{//no tiene hijos

                            nodo = copia->padre;
                            if(nodo == NULL) break;
                            if(pop()) nodo->hijo_der = NULL;
                            else      nodo->hijo_izq = NULL;
                            tamanoPila--;
                            arbol = rotaciones(nodo);
                            break;
                        }
                
            }
            break;
        }
        //**BUSCANDO EL NODO**//
        else if(valor > nodo->valor){
            nodo = nodo->hijo_der;
            es_hijo_derecha = 1;
        }
        else{
            nodo = nodo->hijo_izq;
            es_hijo_derecha = 0;
        }
        push(es_hijo_derecha);
        tamanoPila++;
       }//fin de while
    }
    
}//fin de metodo


struct Nodo *rotaciones(struct Nodo *nodo){
    /**
     * @brief rotaciones splay
     * @param struct Nodo *nodo
     * 
     * realiza las rotaciones splay, existe una pila
     * que recuerda la posicion de los nodos desde la raiz hasta 
     * el nodo que voy a rotar, (osea guarda si son hijos izquierdos, derechos etc)
     * y eso me va a decir que rotacion debo aplica, zig, zig zig, o zig zag
     */
    
        while(tamanoPila){
            
            if(tamanoPila == 1){//ROTACION ZIG
                
                nodo = rotacion_zig(nodo->padre,nodo,pop());
                tamanoPila--;
            }//fin de rotacion zig
            else{
                int hijo_der  = pop();
                int padre_der = pop();
                
                if(hijo_der == padre_der){//ROTACION ZIG ZIG

                    nodo->padre = rotacion_zig(nodo->padre->padre,nodo->padre,padre_der);
                    nodo = rotacion_zig(nodo->padre,nodo,hijo_der);
                    
                }
                else{//ROTACION ZIG ZAG
                    nodo = rotacion_zig(nodo->padre,nodo,hijo_der);
                    nodo = rotacion_zig(nodo->padre,nodo,padre_der);
                }
                tamanoPila-=2;
            }// fin de rotaciones zig zig y zig zag
        }//fin de rotaciones
        //****FIN DE ROTACIONES
    
        return nodo;
}

struct Nodo *rotacion_zig(struct Nodo *raiz, struct Nodo *nodo,int es_hijo_derecha){
    /**
     * @brief rotacion del nodo
     * @param struct Nodo *raiz
     * @param struct Nodo *nodo
     * @param int es_hijo_derecha
     * 
     * rota el nodo del arbol hacia donde corresponde
     * 
     */
    
    if(es_hijo_derecha == 1){//rotacion izquierda
        
        //intercambio de hijos
        raiz->hijo_der = nodo->hijo_izq;
        if(raiz->hijo_der != NULL) raiz->hijo_der->padre = raiz;
        
       //intercambio de padres
       nodo->padre = raiz->padre;
       
       //intercambio final
       raiz->padre = nodo;
       nodo->hijo_izq = raiz;
       return nodo;
       
        
    }
    else{//rotacion hacia derecha
        
        //intercambio de hijos
        raiz->hijo_izq = nodo->hijo_der;//
        if(raiz->hijo_izq != NULL) raiz->hijo_izq->padre = raiz;//
        
        //intercambio de padres
        nodo->padre = raiz->padre;
      
       //intercambio final
       raiz->padre = nodo;
       nodo->hijo_der = raiz;
       
       return  nodo;
       
    }
    return raiz;
    
}


struct Nodo *iniciar_arbol(struct Nodo *padre, int valor,char *pregunta){
    /**
     * @brief incia el arbol
     * @param struct Nodo *padre
     * @param int valor
     * @param char *pregunta
     * 
     * inserta el primer nodo al arbol splay
     * 
     */
    struct Nodo *nuevo_nodo = calloc(sizeof(struct Nodo),1);
    nuevo_nodo->padre = padre;
    nuevo_nodo->valor = valor;
    nuevo_nodo->pregunta = (char*)calloc(200,sizeof(char));
    nuevo_nodo->pregunta = strcpy(nuevo_nodo->pregunta,pregunta);
    return nuevo_nodo;
}

struct nodoHash **agregar(struct nodoHash **v, int *tam){
	/**
	 * @brief agrega las preguntas con sus respectivas respuestas a la tabla de hash
	 * 
	 * @param nodoHash
	 * @param tam
	 * 
	 * Esta función se encarga de recibir la tabla hash, leer de un archivo de texto plano 
	 * las preguntas del juego y agregarlas a la tabla una por una.
	 * 
	 */ 
	
	char* datos_preguntas = (char*)calloc(200,sizeof(char));
    char* respuestaC = (char*)calloc(200,sizeof(char));
    char* respuestaIn = (char*)calloc(200,sizeof(char));
    char* numero = (char*)calloc(200,sizeof(char));
    char* pregunta = (char*)calloc(200,sizeof(char));;
    FILE *f;
    f = fopen("preguntas.txt","r");
    int hash;
    while(!feof(f)){
		
		float fdc = factorDeCarga(v, *tam);
        
		if(fdc < 0.7){
        
			fgets(datos_preguntas,200,f);
            numero = strtok(datos_preguntas,"|");
			
            if(numero == NULL) break;
			pregunta = strtok(NULL,"|");
			respuestaC = strtok(NULL,"|");
			respuestaIn = strtok(NULL,"|");
			
			hash = funcionHash(pregunta, *tam);
            
			v = insertar(v, hash, atoi(numero), pregunta, respuestaC, respuestaIn);
			datos_preguntas = limpiar(datos_preguntas);
			
		}else{
			v = agrandar(v, &*tam);
            datos_preguntas = limpiar(datos_preguntas);
            rewind(f);
		}
	}
    
    return v;
	
}


int funcionHash(char* pregunta, int tam){
	/**
	 * @brief procesa un string y retorna un entero respectivo
	 * 
	 * @param pregunta
	 * @param tam
	 * 
	 * Esta funcion se encarga de recibir un entero correspondiente al tamano de la tabla
	 * además de un puntero que senala a un arreglo de char que corresponde a la pregunta.
	 * Cada elemento del arreglo va siendo convertido a entero y luego sumado en una 
	 * variable local resultado, para despues obtener el modulo de este con el tamano de 
	 * la tabla Hash y retornarlo.
	 * 
	 */ 
	
	int resultado = 0;
	for(int i = 0; i < strlen(pregunta); i++){
		resultado += (int) pregunta[i];
	}
	resultado = resultado % tam;
	return resultado;
}

float factorDeCarga(struct nodoHash **v, int tam){
	/**
	 * @brief calcula el factor de carga de la tabla Hash
	 * 
	 * @param nodoHash
	 * @param tam
	 * 
	 * Esta función se encarga de leer cuantos espacios de la tabla hash han sido ocupados, para
	 * después calcular el factor de carga que corresponde a la division de la cantidad de espacios
	 * llenos entre la cantidad de espacios en la tabla
	 * 
	 */ 
	
	float fdc;
	int i;
	int cont = 0;
	for(i = 0; i < tam; i++){
		if(v[i]->llave != NULL){
			cont++;
		}
	}
	fdc = (float) cont/tam;
	return fdc;
}

struct nodoHash **insertar(struct nodoHash **v, int hash, int id, char* pregunta, char* respuestaC, char* respuestaI){
    /**
	 * @brief inserta las preguntas en la tabla hash dependiendo del valor de hash asignado
	 * 
	 * @param nodoHash
	 * @param hash
	 * @param id
	 * @param pregunta
	 * @param respuestaC
	 * @param respuestaI
	 * 
	 * Esta funcion se encarga de insertar una pregunta con sus respectivas respuestas dentro de la tabla hash. Primero
	 * evalua si el campo respectivo del valor asignado por la funcion Hash, el valor Hash, esta vacio, de estarlo inserta
	 * la pregunta, el id y sus respuestas en el nodo respectivo. De no estarlo, se evalua cual es el ultimo nodo lleno
	 * y se procede a crear un nuevo nodo y enlazarlo a los otros. Retorna la tabla de hash
	 * 
	 */ 

    if(v[hash]->llave == NULL){

        v[hash]->llave = (char*)calloc(200,sizeof(char));
		v[hash]->llave = strcpy(v[hash]->llave,pregunta);
		v[hash]->id = id;
		v[hash]->siguiente = NULL;
        v[hash]->respuestaC = (char*)calloc(200,sizeof(char));
		v[hash]->respuestaC = strcpy(v[hash]->respuestaC,respuestaC);
		v[hash]->respuestaI = (char*)calloc(200,sizeof(char));
        v[hash]->respuestaI = strcpy(v[hash]->respuestaI,respuestaI);
        
    }else{
     
        if(v[hash]->siguiente == NULL){
            struct nodoHash *nuevo_nodo;
            nuevo_nodo = (struct nodoHash*)calloc(sizeof(struct nodoHash),1);
            nuevo_nodo->llave = (char*)calloc(200,sizeof(char));
            nuevo_nodo->llave = strcpy(nuevo_nodo->llave, pregunta);
    
            nuevo_nodo->id = id;
    
            nuevo_nodo->siguiente = NULL;
            nuevo_nodo->respuestaC = (char*)calloc(200,sizeof(char));
            nuevo_nodo->respuestaC = strcpy(nuevo_nodo->respuestaC, respuestaC);
            nuevo_nodo->respuestaI = (char*)calloc(200,sizeof(char));
            nuevo_nodo->respuestaI = strcpy(nuevo_nodo->respuestaI, respuestaI);
            v[hash]->siguiente = nuevo_nodo;
        
        }else{
            struct nodoHash *aux;
            aux = v[hash]->siguiente;
            while(aux -> siguiente != NULL){
                aux = aux->siguiente;
            }
            struct nodoHash *nuevo_nodo;
            nuevo_nodo = (struct nodoHash*)calloc(sizeof(struct nodoHash),1);
            nuevo_nodo->llave = (char*)calloc(200,sizeof(char));
            nuevo_nodo->llave = strcpy(nuevo_nodo->llave, pregunta);
            nuevo_nodo->id = id;
            
            nuevo_nodo->siguiente = NULL;
            nuevo_nodo->respuestaC = (char*)calloc(200,sizeof(char));
            nuevo_nodo->respuestaC = strcpy(nuevo_nodo->respuestaC, respuestaC);
            nuevo_nodo->respuestaI = (char*)calloc(200,sizeof(char));
            nuevo_nodo->respuestaI = strcpy(nuevo_nodo->respuestaI, respuestaI);
            aux->siguiente = nuevo_nodo;
        }
    }
    
    return v;
}

char** buscar(struct nodoHash **v, int tam, char* pregunta, int id){
	/**
	 * @brief busca las respuestas de una pregunta en la tabla hash
	 * 
	 * @param nodoHash
	 * @param tam
	 * @param pregunta
	 * @param id
	 * 
	 * Esta funcion se encarga de encontrar las respuestas para una pregunta determinada. Busca a en el espacio 
	 * correspondiente al valor de hash arrojado por la funcion para la pregunta indicada y recorre la lista enlazada
	 * en caso de no encontrarlo en el primer campo. Compara los ids de la pregunta para ver si se encontro la que se
	 * necesitaba, crea un arreglo con las respuestas y lo retorna.
	 * 
	 */ 
	
	int hash = funcionHash(pregunta, tam);
	
	char** array = (char**)malloc(sizeof(char**) * 2);
	for(int i = 0; i < 2; i++) array[i] = (char*)malloc(sizeof(char) *  200);
	
	if(v[hash]->id == id){ 				// no estoy usando el id, no se si estara bien buscar por la pregunta, creo que no,
												// pero es que de esta manera no tengo que agregarle otro paramentro mas (id) a la funcion
		array[0] = v[hash]->respuestaC;
		array[1] = v[hash]->respuestaI;
		return array;
		
	} else if(v[hash]->siguiente != NULL){
		
        struct nodoHash *aux = (struct nodoHash*)calloc(sizeof(struct nodoHash),1);
		aux = v[hash]->siguiente;
		if(aux->id == id){
			array[0] = aux->respuestaC;
			array[1] = aux->respuestaI;
			return array;
			
		}else{
			
			while(aux->siguiente != NULL){
				aux = aux -> siguiente;
				if(aux->id == id){
					array[0] = aux->respuestaC;
					array[1] = aux->respuestaI;
					return array;
				}
			}
		}
		return NULL;
	} else{
		return NULL;
	}
    return NULL;
}


struct nodoHash **agrandar(struct nodoHash **v, int *tam){
	/**
	 * @brief agranda la tabla hash
	 * 
	 * @param nodoHash
	 * @param tam
	 * 
	 * Esta funcion se encarga de agrandar el arreglo correspondiente a la tabla hash, multiplicando
	 * por 10 el tamano que tenia, no sin antes eliminar los datos que tenia ingresados y volviendo a 
	 * reestablecer el valor NULL de los paramentros respectivos.
	 * 
	 */ 
	 
    *tam += 10;
    v = (struct nodoHash**)realloc(v,*tam*sizeof(struct nodoHash));
    for(int i = 0; i < *tam; i++){
        
        if(i < *tam-10){
            v[i]->siguiente = NULL;
            v[i]->respuestaC = NULL;
            v[i]->respuestaI = NULL;
            v[i]->id = -1;
            v[i]->llave = NULL;
        }
        else{
            v[i] = (struct nodoHash*)calloc(sizeof(struct nodoHash),1);
        }
    }

    return v;
}




